import turtle
import random

def poly(t,sides,length,pc,fc):
	t.pendown()
	t.pencolor(pc)
	t.fillcolor(fc)
	t.begin_fill()
	for n in range (0,sides):
		t.fd(length)
		t.lt(360/sides)
	t.end_fill()
	
def ironman(t,x,y):
	count = 0
	while (count < 100):
		s = random.randint(3,50)
		l = random.randint(3,50)
		x = random.randint(-605,750)
		y = random.randint(-213,750)
		pclr = "#FC00FF"
		fclr =  "#BFE70D"
		t.penup()
		t.goto(x,y)
		poly(t,s,l,pclr,fclr)

def main():
	w = turtle.Screen()
	# setup the screen size
	w.setup(840,840)
	# set the background color
	w.clear()
	w.bgcolor("#D1FF00")
	t = turtle.Turtle()
	ironman(t,0,0)

	w.exitonclick()
	
if __name__ == '__main__':
	main()
